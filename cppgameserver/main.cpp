#include <iostream>
#include "UDPSocket.h"
#include "Room.h"
#if _WIN32
#include "IOCP.h"
#endif
#include "BasicServer.h"
void main() {
	bool isServerRun = true;
	UDPSocket *s = new UDPSocket("0.0.0.0", 51000,1);
	//IOCP *server = new IOCP(s);
	BasicServer *server = new BasicServer(s,1);
	server->start();
	delete server;
	delete s;

}