#include "Room.h"

std::vector<Room*> Room::room_list;
int Room::tmp = 0;
Room::Room()
{
	this->code = tmp++;
	this->isStart = false;
	room_list.push_back(this);
}


Room::~Room()
{
}
void Room::insertPlayer(Session *s) {
	this->player_list.push_back(s);
}
Room* Room::findRoom() {
	for (int i = 0; i < room_list.size(); i++) {
		if (room_list[i]->isStart == false && room_list[i]->player_list.size() < 6) {
			return room_list[i];
		}
	}
	return NULL;
}
Room* Room::getRoomInfo(int code) {
	for (int i = 0; i < room_list.size(); i++) {
		if (room_list[i]->code == code) {
			return room_list[i];
		}
	}
	return NULL;
}
