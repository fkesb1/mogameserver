#include "UDPSocket.h"

UDPSocket::UDPSocket(char *addr,int port,int mode)
{
#if _WIN32
	if (WSAStartup(MAKEWORD(2,2),&(UDPSocket::wsaData))) {
		std::cout << "ERROR STARTUP SOCKET" << std::endl;
		exit(-1);
	}
#endif
	UDPSocket::soc = socket(PF_INET,SOCK_DGRAM,0);
	if (UDPSocket::soc == INVALID_SOCKET) {
		std::cout << "SOCKET ERROR" << std::endl;
		exit(-1);
	}
	UDPSocket::addr.sin_family = AF_INET;
	UDPSocket::addr.sin_port = htons(port);
#if _WIN32
	wchar_t wtext[20];
	mbstowcs_s(NULL,wtext, addr, strlen(addr) + 1);
	LPWSTR ptr = wtext;
	InetPton(AF_INET, ptr, &UDPSocket::addr.sin_addr);
#else
	inet_pton(AF_INET,addr, &UDPSocket::addr.sin_addr);
#endif
	if (bind(UDPSocket::soc, (sockaddr *)&(UDPSocket::addr), sizeof(UDPSocket::addr)) == SOCKET_ERROR)
	{
		std::cout << "BIND ERROR" << std::endl;
		exit(-1);
	}
	std::cout << "UDP SOCKET started" << std::endl;
}
void UDPSocket::setNonBlock() {
#if _WIN32
	u_long arg = 1;
	ioctlsocket(UDPSocket::soc, FIONBIO, &arg);
#else
	int flag = fcntl(UDPSocket::soc, F_GETFL, 0);
	fcntl(UDPSocket::soc, F_SETFL, flag);
#endif
}
int UDPSocket::recvFrom(char *recv,SOCKADDR_IN *addr) {
#if _WIN32
	int size = sizeof(clntaddr);
	WSABUF buf;
	DWORD recvSize;
	DWORD flag = 0;
	buf.buf = recv;
	buf.len = 1024;
	OVERLAPPED o;
	ZeroMemory(&o, sizeof(o));
	int re = WSARecvFrom(soc, &buf, 1,&recvSize, &flag, (SOCKADDR*)&clntaddr, &size,NULL, 0);
	if (re == 0) {
		*addr = clntaddr;
		recv = buf.buf;
		return recvSize;
	}
	else{
		return -1;
	}
#else
	int size = sizeof(clntaddr);
	int recvLength = recvfrom(soc, recv, 1024, 0, (SOCKADDR*)&clntaddr, &size);
	if (recvLength >= 0) {
		*addr = clntaddr;
	}
	return recvLength;
#endif
}
int UDPSocket::sendTo(char *data,SOCKADDR_IN addr) {
#if _WIN32
	WSABUF buf;
	buf.buf = data;
	buf.len = strlen(data);
	int re = WSASendTo(soc, &buf, 1, NULL, 0, (SOCKADDR*)&addr, sizeof(addr),NULL,0);
	return re;
#else
	int size = sendto(soc, recv, len, 0, (SOCKADDR*)&addr, sizeof(addr));
	return size;
#endif

}
UDPSocket::~UDPSocket()
{
#if _WIN32
	closesocket(soc);
	WSACleanup();
#else
	close(soc);
#endif
}
