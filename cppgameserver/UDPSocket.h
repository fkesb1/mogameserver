#pragma once
#pragma comment(lib,"ws2_32.lib")
#include <WinSock2.h>
#include <iostream>
#include <Ws2tcpip.h>
#include "json\json.h"
class UDPSocket
{
public:
	UDPSocket(char *,int,int);
	~UDPSocket();
	WSADATA wsaData;
	SOCKET soc;
	SOCKADDR_IN addr,clntaddr;

	int sendTo(char *, SOCKADDR_IN );
	int recvFrom(char *,SOCKADDR_IN *);
	void setNonBlock();
};

