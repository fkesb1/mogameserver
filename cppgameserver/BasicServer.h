#pragma once
#include "UDPSocket.h"
#include "Session.h"
#include "json\json.h"
#include "PacketType.h"
#include "Room.h"
#include <queue>
#include <mutex>
class BasicServer
{
public:
	BasicServer(UDPSocket*,int);
	~BasicServer();
	static UDPSocket* udp;
	static void start();
	static std::queue<char*> buffer;
#if _WIN32
	static DWORD WINAPI serverFunc(LPVOID);
	static DWORD WINAPI sendFunc(LPVOID);
	static DWORD WINAPI SessionManager(LPVOID);
	static void parse();
	static void safeEnque(char *);
	static char* safeDeque(Json::Value*);
	static void multicast(char *, std::vector<Session*>);
	static void multicast(char *, std::vector<Session*>,SOCKADDR_IN*);
private:
#else

#endif
};

