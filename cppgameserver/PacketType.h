#pragma once
enum PacketType {
	OP_Dummy,
	OP_Letancy,
	OP_LetancyReply,
	OP_Hello,
	OP_HelloReply,
	OP_FindRoom,
	OP_FindRoomReply,
	OP_PlayerMove,
	OP_Bye
};