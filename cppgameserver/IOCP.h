#pragma once
#include "UDPSocket.h"
#include <iostream>

class IOCP
{
public:
	IOCP(UDPSocket *);
	~IOCP();
private:
	UDPSocket *UdpSock;
	static HANDLE hcp;
	HANDLE hThread;
	static DWORD WINAPI WorkerThread(LPVOID);
	static void recv(UDPSocket*);
};

