#include "IOCP.h"


HANDLE IOCP::hcp = NULL;
IOCP::IOCP(UDPSocket *UdpSoc)
{
	this->UdpSock = UdpSoc;
	IOCP::hcp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	CreateIoCompletionPort((HANDLE)UdpSock->soc, hcp, (DWORD)UdpSock->soc, 0);
	hThread = CreateThread(NULL, 0, WorkerThread, NULL, 0, 0);
	recv(UdpSock);
	char szInput[128];
	while (true)
	{
		scanf_s("%s", szInput,19);
		fflush(stdin);
		if (!strncmp(szInput, "exit", (int)strlen("exit"))) {
			break;
		}
	}
}

DWORD WINAPI IOCP::WorkerThread(LPVOID arg) {
	int retval;
	SOCKADDR_IN addr;
	char msg[1024] = "";
	DWORD dwBytesTransfer, CompletionKey;
	LPOVERLAPPED lpOverlapp;
	UDPSocket *udp = (UDPSocket*)arg;
	int check=-1;
	while (true) {
		check=GetQueuedCompletionStatus(IOCP::hcp, &dwBytesTransfer, &CompletionKey, &lpOverlapp, INFINITE);
		if (!check) {
			std::cout << "err" << std::endl;
			continue;
		}
		recv(udp);
	}
}

void IOCP::recv(UDPSocket *udp) {
	SOCKADDR_IN addr;
	char msg[1024] = "";
	int size = udp->recvFrom(msg, &addr);
	if (size >= 0) {
		std::cout << msg << std::endl;
	}
}

IOCP::~IOCP()
{
}
