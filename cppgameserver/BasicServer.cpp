#include "BasicServer.h"


std::queue<char*> BasicServer::buffer;
UDPSocket* BasicServer::udp;
BasicServer::BasicServer(UDPSocket* udp,int flag)
{
	this->udp = udp;
	if (flag == 1) {
		udp->setNonBlock();
	}
}
void BasicServer::start() {
	char command[30];
#if _WIN32
	CreateThread(NULL, 0, serverFunc, NULL, 0, 0);
#else

#endif
	while (true) {
		std::cin >> command;
		if (!strcmp(command, "2xit")) {
			break;
		}
	}
}
DWORD WINAPI BasicServer::serverFunc(LPVOID args) {
	Json::Reader reader;
	Json::Value root,root2;
	Json::StyledWriter writer;
	while (true) {
		char msg[1024] = "";
		SOCKADDR_IN sockaddr;
		int size = udp->recvFrom(msg,&sockaddr);
		if (size > 0) {
			if (!reader.parse(msg, root)) {
				printf("error parse data %s\n",msg);
				continue;
			}
			switch (root["packetType"].asInt()) {
				case PacketType::OP_Hello: {
					Session *s = new Session(sockaddr);
					root2["packetType"] = PacketType::OP_HelloReply;
					root2["key"] = s->key;
					udp->sendTo(&(writer.write(root2))[0u],s->addr);
				}; break;
				case PacketType::OP_FindRoom: {
					Session *s = Session::findSession(root["key"].asInt());
					if (s == nullptr) {
						continue;
					}
					Room *r=Room::findRoom();
					if (r == nullptr) {
						r = new Room();
						r->insertPlayer(s);
					}
					else {
						r->insertPlayer(s);
					}
					root2["packetType"] = PacketType::OP_FindRoomReply;
					root2["code"] = r->code;
					udp->sendTo(&(writer.write(root2))[0u], s->addr);
				}; break;
				case PacketType::OP_PlayerMove: {
					Room *r = Room::getRoomInfo(root2["code"].asInt());
					multicast(msg, r->player_list,&sockaddr);
				}; break;
				case PacketType::OP_Bye: {
					int key = root["key"].asInt();
					Session::destroy(key);
				}; break;
			}
		}
	}
}
DWORD WINAPI BasicServer::SessionManager(LPVOID) {
	while (true) {
		Sleep(300000);
		//kill ghost user and ghost room;
		int size = Session::session_list.size();
		for (int i = 0; i < size; i++) {
			
		}
		size = Room::room_list.size();
		for (int i = 0; i < size; i++) {

		}
	}
}
void BasicServer::multicast(char *data, std::vector<Session*> list) {
	
}
void BasicServer::multicast(char *data, std::vector<Session*> list,SOCKADDR_IN* addr) {
	for (int i = 0; i < list.size(); i++) {
		if (list[i]->addr.sin_addr.S_un.S_addr == addr->sin_addr.S_un.S_addr && list[i]->addr.sin_port == addr->sin_port) {
			continue;
		}
		int re=udp->sendTo(data, list[i]->addr);
	}
}
BasicServer::~BasicServer()
{

}
