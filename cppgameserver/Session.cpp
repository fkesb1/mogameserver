#include "Session.h"


int Session::tmp = 0;
std::vector<Session*> Session::session_list;
Session::Session(SOCKADDR_IN addr)
{
	tmp++;
	for (int i = 0; i < session_list.size(); i++) {
		if (session_list[i]->addr.sin_addr.S_un.S_addr == addr.sin_addr.S_un.S_addr && session_list[i]->addr.sin_port == addr.sin_port) {
			session_list.erase(session_list.begin() + i);
			break;
		}
	}
	this->addr = addr;
	this->key = tmp;
	this->time = std::chrono::system_clock::now();
	session_list.push_back(this);
}


Session::~Session()
{
	for (int i = 0; i < session_list.size(); i++) {
		if (this == session_list[i]) {
			session_list.erase(session_list.begin() + i);
			break;
		}
	}
}

Session* Session::findSession(int key) {
	for (int i = 0; i < session_list.size(); i++) {
		if (session_list[i]->key == key) {
			return session_list[i];
		}
	}
	return NULL;
}
void Session::destroy(int key) {
	for (int i = 0; i < session_list.size(); i++) {
		if (session_list[i]->key == key) {
			delete session_list[i];
			session_list.erase(session_list.begin()+i);
		}
	}
}