#pragma once
#include <winsock2.h>
#include <vector>
#include <chrono>
#include <iostream>
class Session
{
public:
	Session(SOCKADDR_IN);
	~Session();
	int key;
	SOCKADDR_IN addr;
	std::chrono::system_clock::time_point time;
	static int tmp;
	static void destroy(int);
	static std::vector<Session*> session_list;
	static Session* findSession(int);
};

