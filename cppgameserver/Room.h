#pragma once
#include <vector>
#include <iostream>
#include "Session.h"
class Room
{
public:
	Room();
	~Room();
	std::vector<Session*> player_list;
	static std::vector<Room*> room_list;
	bool isStart;
	int code;
	void insertPlayer(Session*);
	static Room* findRoom();
	static Room* getRoomInfo(int);
	static int tmp;
};

